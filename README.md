# vuetest-hoodie

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8085
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run the hoodie server at localhost:8080
npm run start-hoodie
```

PS. I've run the vue-project without running the hoodie server and
the data saving still worked. I'm somewhat confused because of it.